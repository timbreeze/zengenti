function initMap() {
  var render = document.getElementById("map__render");
  var location = { lat: 51.4958306, lng: -0.3405778 };
  var map = new google.maps.Map(render, {
    zoom: 12,
    center: location,
    disableDefaultUI: true,
    scrollwheel: false,
    disableDoubleClickZoom: true,
    styles: [
      {
        featureType: "all",
        elementType: "geometry",
        stylers: [{ lightness: "40" }],
      },
      {
        featureType: "landscape.natural",
        elementType: "geometry.fill",
        stylers: [
          { visibility: "on" },
          { color: "#92c83e" },
          { lightness: "70" },
          { saturation: "-25" },
        ],
      },
      {
        featureType: "road",
        elementType: "labels",
        stylers: [{ visibility: "off" }],
      },
      {
        featureType: "road.highway",
        elementType: "geometry.fill",
        stylers: [{ visibility: "on" }, { color: "#d2d2d2" }],
      },
      {
        featureType: "road.highway",
        elementType: "geometry.stroke",
        stylers: [{ color: "#bebebe" }, { visibility: "off" }],
      },
      {
        featureType: "road.highway.controlled_access",
        elementType: "geometry.fill",
        stylers: [{ visibility: "on" }, { color: "#b9b9b9" }],
      },
      {
        featureType: "road.highway.controlled_access",
        elementType: "geometry.stroke",
        stylers: [{ color: "#d2d2d2" }, { visibility: "on" }],
      },
      {
        featureType: "water",
        elementType: "all",
        stylers: [
          { color: "#46acb9" },
          { lightness: "65" },
          { saturation: "-15" },
        ],
      },
    ],
  });
  var icon = {
    url: "img/map-pin@2x.png",
    scaledSize: new google.maps.Size(80, 80),
    anchor: new google.maps.Point(40, 40),
  };
  var marker = new google.maps.Marker({
    position: location,
    map: map,
    icon: icon,
  });
}

// hasClass - helper function
function hasClass(ele, cls) {
  return !!ele.className.match(new RegExp("(\\s|^)" + cls + "(\\s|$)"));
}

// addClass - helper function
function addClass(ele, cls) {
  if (!hasClass(ele, cls)) ele.className += " " + cls;
}

// removeClass - helper function
function removeClass(ele, cls) {
  if (hasClass(ele, cls)) {
    var reg = new RegExp("(\\s|^)" + cls + "(\\s|$)");
    ele.className = ele.className.replace(reg, " ");
  }
}

// Toggles the navigation on mobile
function toggleNavigation() {
  var navToggle = document.getElementById("navToggle");
  var nav = document.getElementById("navigation");
  var searchToggle = document.getElementById("searchToggle");
  var searchForm = document.getElementById("searchForm");
  if (hasClass(nav, "active")) {
    removeClass(nav, "active");
    removeClass(navToggle, "open");
  } else {
    removeClass(nav, "s-open");
    removeClass(searchForm, "s-visible");
    addClass(nav, "active");
    addClass(navToggle, "open");
  }
}

// Toggles aria expanded for the main-navigation
function toggleAria() {
  var mainNav = document.getElementById("mainNav");
  var mainNavAria = mainNav.getAttribute("aria-expanded");
  if (mainNavAria == "true") {
    mainNavAria = "false";
  } else {
    mainNavAria = "true";
  }
  mainNav.setAttribute("aria-expanded", mainNavAria);
}

if (navToggle.addEventListener) {
  navToggle.addEventListener("click", toggleNavigation);
  navToggle.addEventListener("click", toggleAria);
} else {
  navToggle.attachEvent("onclick", toggleNavigation);
  navToggle.attachEvent("onclick", toggleAria);
}

// Toggles search window
function toggleSearch() {
  var navToggle = document.getElementById("navToggle");
  var nav = document.getElementById("navigation");
  var searchToggle = document.getElementById("searchToggle");
  var searchForm = document.getElementById("searchForm");
  if (hasClass(nav, "s-open")) {
    removeClass(nav, "s-open");
    removeClass(searchForm, "s-visible");
  } else {
    removeClass(nav, "active");
    removeClass(navToggle, "open");
    addClass(nav, "s-open");
    addClass(searchForm, "s-visible");
  }
}

if (searchToggle.addEventListener) {
  searchToggle.addEventListener("click", toggleSearch);
} else {
  searchToggle.attachEvent("onclick", toggleSearch);
}

// Dropdown Toggle
var dropDown = document.getElementById("dropDown");
var dropDownMenu = document.querySelectorAll(".dropdown__menu");

if (dropDown.addEventListener) {
  dropDown.addEventListener("click", function(e) {
    dropDownMenu[0].style.display = "block";
  });
  dropDown.addEventListener("mouseenter", function(e) {
    dropDownMenu[0].style.display = "block";
  });
  dropDown.addEventListener("mouseleave", function(e) {
    dropDownMenu[0].style.display = "";
  });
} else {
  dropDown.attachEvent("onclick", function(e) {
    dropDownMenu[0].style.display = "block";
  });
  dropDown.attachEvent("onmouseenter", function(e) {
    dropDownMenu[0].style.display = "block";
  });
  dropDown.attachEvent("onmouseleave", function(e) {
    dropDownMenu[0].style.display = "";
  });
}
